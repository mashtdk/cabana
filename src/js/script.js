$(document).ready(function(){
	$('body').on('dragstart', function(event) { event.preventDefault(); });

	$('a[href*=#]:not([href=#])').click(function() {
		var distance = $('#cloud').offset().top - $(window).height();

		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
			|| location.hostname == this.hostname) {

			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top - distance
				}, 700);
				return false;
			}
		}

	});

	$(window).scroll(function() {
		if ($(this).scrollTop() > $(window).height()){  
		    $('.menu-bar').addClass("fixed");
		  }
	  else{
	    $('.menu-bar').removeClass("fixed");
	  }
	})
});
